const mongoose = require('mongoose');
const { Schema } = mongoose;
//Creating a schema model for user information to POST to the database
const advertSchema = new Schema({
    Url: { type: String },
    picture: { type: String },
    status: { type: String, default: "draft", enum: ["published", "draft", "expired"] },
    isDraft: { type: Boolean, default: true },
    author: [{ type: Schema.Types.ObjectId, ref: "User" }],
}, { timestamps: { createdAt: 'createdAt' } });

module.exports = mongoose.model('advert', advertSchema);//Exporting the model to be available to routes