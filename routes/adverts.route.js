const advert = require('../models/adverts.model');
const router = require('express').Router();

//Creating a POST endpoint
    router.post('/api/advert', (req, res, next)=>{
    let new_advert = new Article({
        Url:req.body.Url,
        status:req.body.status,
        picture: req.body.picture,
        });
        new_advert.save(err=>{
        if(err){console.log(err)}
        res.json({response:"New advert created"})
    });
});

//creating a GET articles endpoint to get/retrive all information from DB
router.get('/api/advert', (req, res,next)=>{
    //Function to get all articles from a database that were created based on the UserSchema
    advert.find()
     .exec((err,advert)=>{;
        if (err) return next(err);
        res.json(advert);
    });
});

//Request for getting a single article (GET single article)
router.get('/api/advert/:id', function(req, res){
    Article.findOne({_id:req.params.id}, function(err,foundAdvert){
        if(err) return next(err);
        res.json(foundAdvert);
    });
});

router.get('/api/advert/q', function(req, res){
    advert.find({_id:req.params.id}, function(err,foundAdvert){
        if(err) return next(err);
        res.json(foundAdvert);
    });
});

//Request for and deleting an article (by single article)
router.delete('/api/advert/:id', function(req, res){
    advert.findByIdAndRemove({_id:req.params.id}, function(err,foundAdvert){
        if(err) return next(err);
        res.json(foundAdvert);
    });
});

//Creating an update request for the category using PUT
router.put('/api/advert/:id', function(req,res,next){
    advert.findById(req.params.id, function(err,foundAdvert){
        if(err) return next(err);
        if(req.body.title){
            foundAdvert.title = req.body.title;
        }
        if(req.body.body){
            foundAdvert.body = req.body.body;
        }
        foundAdvert.save(function(err, updatedAdvert){
            if (err) return next(err);
            let obj = {
                message:"advert updated successfully",
                updatedAdvert: updatedAdvert
            }
            res.json(obj)
        });
    });
});
module.exports = router;